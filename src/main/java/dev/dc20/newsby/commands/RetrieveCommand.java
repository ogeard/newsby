package dev.dc20.newsby.commands;

import discord4j.core.event.domain.interaction.ChatInputInteractionEvent;
import discord4j.core.object.command.ApplicationCommandInteractionOption;
import discord4j.core.object.command.ApplicationCommandInteractionOptionValue;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

@Component
public class RetrieveCommand implements SlashCommand {
    @Override
    public String getName() {
        return "retrieve";
    }

    @Override
    public Mono<Void> handle(ChatInputInteractionEvent event) {

        String uri = event.getOption("url")
            .flatMap(ApplicationCommandInteractionOption::getValue)
            .map(ApplicationCommandInteractionOptionValue::asString)
            .get();

        try {
            URL url = new URL(uri);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setConnectTimeout(5000);
            con.setReadTimeout(5000);
            int status = con.getResponseCode();
            BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder content = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }

            in.close();
            con.disconnect();

            String JustDoIt = content.toString();

            return event.reply()
                .withEphemeral(true)
                .withContent("Response code %s".formatted(status));
        } catch (MalformedURLException malformedURLException) {
            return event.reply()
                .withEphemeral(true)
                .withContent("Invalid URI");
        } catch (Exception ignored) {
            return event.reply()
                .withEphemeral(true)
                .withContent("Error encountered.");
        }
    }
}
