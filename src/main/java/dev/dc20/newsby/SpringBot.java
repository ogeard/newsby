package dev.dc20.newsby;

import dev.dc20.newsby.listeners.SlashCommandListener;
import discord4j.core.DiscordClientBuilder;
import discord4j.core.event.domain.interaction.ChatInputInteractionEvent;
import discord4j.rest.RestClient;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import reactor.core.publisher.Mono;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@SpringBootApplication
public class SpringBot {
    //Testing gitlab security scanning
    private static final Logger logger = LogManager.getLogger(SpringBot.class);

    public static void main(String[] args) {
        logger.debug("Hello Gitlab");
        logger.info("Will you yell at me?");

        //Start spring application
        ApplicationContext springContext = new SpringApplicationBuilder(SpringBot.class)
            .build()
            .run(args);

        //Login
        DiscordClientBuilder.create(System.getenv("BOT_TOKEN")).build()
            .withGateway(gatewayClient -> {
                SlashCommandListener slashCommandListener = new SlashCommandListener(springContext);

                Mono<Void> onSlashCommandMono = gatewayClient
                    .on(ChatInputInteractionEvent.class, slashCommandListener::handle)
                    .then();

                return Mono.when(onSlashCommandMono);
            }).block();
    }


    @Bean
    public RestClient discordRestClient() {
        return RestClient.create(System.getenv("BOT_TOKEN"));
    }
}
